package AR.Effects;

import AR.Const;
import lejos.nxt.NXTMotor; // rozhraní portu
import lejos.util.Delay; // sleep

/**
 * Blikač. Funguje tak, že se spustí vlákno na pozadí a to bliká světlem na portu C.
 */
public class Blikac extends Thread {
	/**
	 * Dolní procentní limit
	 */
	public static final int LWM = 0;
	/**
	 * Horní procentní limit
	 */
	public static final int HWM = 100;
	/**
	 * Procentní posun
	 */
	public static final int MOVE = 5;
	/**
	 * Prodleva mezi cykly
	 */
	public static final int WAIT = 5;
	/**
	 * kam míříme?
	 */
	public boolean up=true;
	/**
	 * aktuální výkon
	 */
	public int power = LWM;



	/**
	 * Fyzický port (výstup)
	 */
	NXTMotor m;
	/**
	 * Rodičovské vlákno
	 */
	Thread parent;



	/**
	 * Základní konstruktor
	 * @param origin Rodičovský konstruktor
	 */
	public Blikac(Thread origin)
	{
		m = new NXTMotor(Const.blikac); // nastav výstup
		m.setPower(power); // nastav počáteční výkon
		parent = origin; // nastav rodičovské vlákno
	}

	/**
	 * Program vlákna
	 */
	@Override
	public void run() {
		while (parent.isAlive()) { // opakuj dokud rodičovské vlákno pracuje
			if (up) { // pokud jdeme nahoru
				if (power < HWM) // pokud jsme v limitu
					m.setPower(power += MOVE); // zvyš výkon
				else // jinak
					up = false; // jdeme dolu
			} else { // pokud jdeme dolu
				if (power > LWM) // pokud jsme v limitu
					m.setPower(power -= MOVE); // sniž výkon
				else // jinak
					up = true; // jdeme nahoru
			}
			Delay.msDelay(WAIT); // čekej
		}
	}

}
