package AR;

import lejos.nxt.Button;
import lejos.nxt.MotorPort;
import lejos.nxt.SensorPort;

/**
 * Konstanty, na toto by se hodil .h soubor :)
 */
public final class Const {
	private Const(){}
	/**
	 * Jak dlouhé je jedno pole
	 */
	public static final int TILE_LENGTH = 28;
	/**
	 * Maximální počet volných polí, kam může ultrazvukový senzor vidět
	 */
	public static final short US_LIMIT_COEF = 5;
	/**
	 * Rychlost robota v centrimetrech za sekundu (cm/s)
	 */
	public static final int SPEED = 30;
	/**
	 * Rychlost robota při jízdě dopředu
	 */
	public static final int FORWARDSPEED = SPEED;
	/**
	 * Úhlové zrychlení motoru
	 */
	public static final int ACCEL = 2000;
	/**
	 * Průměr kola
	 */
	public static final float WHEELDIAMETER = 8.3f;
	/**
	 * Rozchod, levé <-> pravé
	 */
	public static final float TRACKWIDTH = 14.8f;
	/**
	 * Rozvor, přední <-> zadní
	 */
	public static final float WHEELBASE = 9.5f;
	/**
	 * Port ultrazvukového senzoru
	 */
	public static final SensorPort sonicPort = SensorPort.S1;
	/**
	 * Port gyroskopu
	 */
	public static final SensorPort gyroPort = SensorPort.S2;
	/**
	 * Port tlačítka
	 */
	public static final SensorPort touchPort1 = SensorPort.S3; // where is plugged touch sensor
	/**
	 * Port tlačítka
	 */
	public static final SensorPort touchPort2 = SensorPort.S4; // where is plugged touch sensor
	/**
	 * Port levého motoru
	 */
	public static final MotorPort leftMotor = MotorPort.A;
	/**
	 * Port pravého motoru
	 */
	public static final MotorPort rightMotor = MotorPort.B;
	/**
	 * Port blikače
	 */
	public static final MotorPort blikac = MotorPort.C;
	/**
	 * Zabíjecí tlačítko
	 */
	public static final Button killButton = Button.ESCAPE;
}
