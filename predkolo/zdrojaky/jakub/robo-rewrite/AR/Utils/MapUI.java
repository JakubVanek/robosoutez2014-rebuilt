package AR.Utils;

import lejos.nxt.Button; // tlačítko
import lejos.nxt.ButtonListener; // obsluha událostí tlačítka
import lejos.nxt.LCD; // displej

/**
 * Menu pro výběr čísla mapy
 */
public final class MapUI {
	/**
	 * Aktuální hodnota menu
	 */
	public static int value = 1;
	/**
	 * Zámek, slouží pro čekání do zmáčknutí ENTERu
	 */
	public static final Object lock = new Object();
	/**
	 * Nejmenší možná odpověď
	 */
	public static final int minNumber = 1;

	/**
	 * Zobrazí menu, počká na zmáčknutí ENTERu a vrátí zadané číslo
	 * @return Zadané číslo
 	 */
	public static int getNumber(final int maxNumber) // největší možná odpověď
	{
		LCD.clear(); // vyčistí displej
		LCD.drawString("Select map:",0,0); // napíše výzvu
		value = 1; // resetuje hodnotu
		write(); // vypíše hodnotu
		Button.LEFT.addButtonListener(new ButtonListener() { // nastaví obsluhu událostí tlačítka doleva
			@Override
			public void buttonPressed(Button b) { // při zmáčknutí
				if(value == minNumber){ // pokud jsme na minimu
					value = maxNumber; // skočí na maximum
				}else{ // pokud můžeme níž
					value--; // sníží aktuálně zadanou hodnotu
				}
				write(); // přepíše starou hodnotu
			}

			@Override
			public void buttonReleased(Button b) {} // při uvolnění nic
		});
		Button.RIGHT.addButtonListener(new ButtonListener() { // nastaví obsluhu událostí pravého tlačítka
			@Override
			public void buttonPressed(Button b) { // při zmáčknutí
				if(value == maxNumber){ // pokud jsme na maximu
					value = minNumber; // skočí na minimum
				}else{ // pokud můžeme výš
					value++; // zvýší aktuálně zadanou hodnotu
				}
				write(); // přepíše starou hodnotu
			}

			@Override
			public void buttonReleased(Button b) {} // při uvolnění nic
		});
		Button.ENTER.addButtonListener(new ButtonListener() {// nastaví obsluhu událostí ENTERu
			@Override
			public void buttonPressed(Button b) { // při zmáčknutí
				synchronized (lock) { // synchronizuje se zámkem
					lock.notify(); // uvolní zámek
				}
			}

			@Override
			public void buttonReleased(Button b) {} // při uvolnění nic
		});
		try { // zachytí případnou vyjímku
			synchronized (lock) { // synchronizuje se zámkem
				lock.wait(); // počká do uvolnění zámku ENTERem
			}
		} catch (InterruptedException e) { // při vyjímce
			e.printStackTrace(); // vypíše tzv. stack trace
		}
		return value; // vrátí hodnotu
	}

	/**
	 * Výpíše hodnotu na displej
	 */
	private static void write() //
	{
		LCD.drawString(value + "  ", 5, 1); // Doprostřed napíše číslo a smaže znaky za ním
	}
}
