package nxjsim.AR;
import java.io.*;
import java.net.URI;
import java.util.*;

/**
 * nxjsim.AR.Map
 * 
 * @author Jakub Vaněk
 */
public class Map {
	// VARIABLES & CONSTANTS
	/**
	 * X size of map (horizontal)
	 */
	public final int sizeX = 11;
	/**
	 * Y size of map (vertical)
	 */
	public final int sizeY = 8;
	/**
	 * Location of map center (start)
	 */
	public final Pos center = new Pos(5, 4);
	/**
	 * Physical map
	 */
	public int[][] map;
	/*
	 * Current orientation
	 */
	public int curOrient = Orient.North;
	/**
	 * Current position
	 */
	public Pos curPos = new Pos(center.x, center.y);
	/**
	 * The maximum number of walls excluding start and borders
	 */
	public final int maxWalls = 11;
	/**
	 * Current number of detected walls
	 */
	public int curWalls = 0;
	
	// FUNCTIONS
	/**
	 * Constructor
	 */
	public Map() {
		// Vytvořit mapu
		map = new int[sizeX][sizeY];
		// Naplnit mapu neznámými bloky
		for (int[] part : map)
			Arrays.fill(part, Tile.Unknown);
		// Nakreslit okraje
		for (int x = 0; x < sizeX; x++) {
			map[x][0] = Tile.Wall;
			map[x][sizeY - 1] = Tile.Wall;
		}
		for (int y = 1; y < sizeY - 1; y++) {
			map[0][y] = Tile.Wall;
			map[sizeX - 1][y] = Tile.Wall;
		}
		// start a bloky kolem cíle jsou také zdi, nezapočítat
		map[center.x - 1][center.y] = Tile.Wall;
		map[center.x][center.y] = Tile.Wall;
		map[center.x + 1][center.y] = Tile.Wall;
	}
	
	public void load(String name) throws Exception {
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream fstr = classLoader.getResourceAsStream(name);
		char[] all = new char[88];
		int index = 0;
		while(fstr.available()>0 && index < 88)
		{
			int result=fstr.read();
			if(result!=-1)
				all[index]=(char)result;
			else
			break;
				index++;
		}
		fstr.close();
		index = 0;
		for (int y = 0; y < sizeY; y++) {
			for (int x = 0; x < sizeX; x++) {
				switch (all[index]) {
					case 'X':
						map[x][y] = Tile.Wall;
						break;
					case '_':
						map[x][y] = Tile.LightOn;
						break;
					default:
						map[x][y] = Tile.LightOn;
						break;
				}
				index++;
			}
		}
		curWalls = maxWalls; // THE MAP MUST BE CORRECTLY ENTERED!!!
	}

	/**
	 * 
	 * @param toRight
	 *            Whether to turn to right
	 * @param count
	 *            Count of quarters of 360° to turn
	 */
	public void turn(boolean toRight, int count) {
		if (!toRight)
			count *= -1;
		curOrient = Orient.Edit(curOrient, count);
	}

	/**
	 * Straight travel by specified count of tiles.
	 * 
	 * @param count
	 *            Count of tiles to travel
	 * @param backward
	 *            Whether to go backward or not
	 */
	public void travel(int count, boolean backward) {
		// orientace
		int myOrient = curOrient;
		// zde byl bug, pokud jedeme dozadu, "otočit se"
		if (backward)
			myOrient = Orient.Edit(curOrient, 2);
		switch (myOrient) {
		case Orient.North:
			for (int y = curPos.y, i = 0; i <= count; i++, y--)
				if(map[curPos.x][y] == Tile.Unknown || map[curPos.x][y] == Tile.LightOn) map[curPos.x][y] = Tile.LightOff;
			break;
		case Orient.East:
			for (int x = curPos.x, i = 0; i <= count; i++, x++)
                if(map[x][curPos.y] == Tile.Unknown || map[x][curPos.y] == Tile.LightOn) map[x][curPos.y] = Tile.LightOff;
			break;
		case Orient.South:
			for (int y = curPos.y, i = 0; i <= count; i++, y++)
                if(map[curPos.x][y] == Tile.Unknown || map[curPos.x][y] == Tile.LightOn) map[curPos.x][y] = Tile.LightOff;
			break;
		case Orient.West:
			for (int x = curPos.x, i = 0; i <= count; i++, x--)
                if(map[x][curPos.y] == Tile.Unknown || map[x][curPos.y] == Tile.LightOn) map[x][curPos.y] = Tile.LightOff;
			break;
		}
		curPos = Pos.modify(curPos, myOrient, count);
	}

	/**
	 * Writes to map detected non-blocking tiles.
	 * @param freeTiles How many free tiles were detected
	 * @param USorient Orientation of ultrasonic sensor
	 * @param wall Whether there is a wall at the end
	 */
	public void writeDetection(int freeTiles, int USorient, boolean wall)  throws InterruptedException
	{
		int abs = Orient.usEdit(curOrient, USorient);
		try {
			switch (abs) {
				case Orient.North:
					for (int y = curPos.y, i = 0; i <= freeTiles + 1; i++, y--) {
						if (i < freeTiles + 1) {
							if (map[curPos.x][y] == Tile.Unknown)
								map[curPos.x][y] = Tile.LightOn;
						} else {
							if (map[curPos.x][y] == Tile.Unknown && wall) {
								map[curPos.x][y] = Tile.Wall;
								curWalls++;
							}
						}
					}
					break;

				case Orient.East:
					for (int x = curPos.x, i = 0; i <= freeTiles + 1; i++, x++) {
						if (i < freeTiles + 1) {
							if (map[x][curPos.y] == Tile.Unknown)
								map[x][curPos.y] = Tile.LightOn;
						} else {
							if (map[x][curPos.y] == Tile.Unknown && wall) {
								map[x][curPos.y] = Tile.Wall;
								curWalls++;
							}
						}
					}
					break;

				case Orient.South:
					for (int y = curPos.y, i = 0; i <= freeTiles + 1; i++, y++) {
						if (i < freeTiles + 1) {
							if (map[curPos.x][y] == Tile.Unknown)
								map[curPos.x][y] = Tile.LightOn;
						} else {
							if (map[curPos.x][y] == Tile.Unknown && wall) {
								map[curPos.x][y] = Tile.Wall;
								curWalls++;
							}
						}
					}
					break;

				case Orient.West:
					for (int x = curPos.x, i = 0; i <= freeTiles + 1; i++, x--) {
						if (i < freeTiles + 1) {
							if (map[x][curPos.y] == Tile.Unknown)
								map[x][curPos.y] = Tile.LightOn;
						} else {
							if (map[x][curPos.y] == Tile.Unknown && wall) {
								map[x][curPos.y] = Tile.Wall;
								curWalls++;
							}
						}
					}
					break;
			}
		}
		catch(ArrayIndexOutOfBoundsException e) // This shit is occuring while testing with US_LIMIT_COEF
		{
			System.err.println("writeDetection(): array index overflow!");
		}
		// pokud byly objeveny všechny zdi, nahraď všechny neznámé svítícím
		// světlem
		if (curWalls >= maxWalls)
			for (int x = 0; x < sizeX; x++)
				for (int y = 0; y < sizeY; y++)
					if (map[x][y] == Tile.Unknown)
						map[x][y] = Tile.LightOn;
	}
	
	/**
	 * How many tiles are before a wall
	 * 
	 * @param right
	 *            Whether to scan to right
	 * @param tile
	 *            Tile
	 * @return Count of tiles
	 */
	public int beforeWall(boolean right, int tile) {
		int orient = right ? Orient.Edit(curOrient, 1) : Orient.Edit(curOrient, -1);
		return beforeWall(orient,tile);
	}
	/**
	 * How many tiles are before a wall
	 *
	 * @param orient
	 *            Where to scan
	 * @param tile
	 *            Tile
	 * @return Count of tiles
	 */
	public int beforeWall(int orient, int tile) {
		int retVal = 0;

		switch (orient) {
			case Orient.North:
				for (int i = 1, y = curPos.y - 1; true; i++, y--) {
					if (map[curPos.x][y] == Tile.Wall)
						break;
					if(map[curPos.x][y] == tile)
						retVal++;
				}
				break;
			case Orient.East:
				for (int i = 1, x = curPos.x + 1; true; i++, x++) {
					if (map[x][curPos.y] == Tile.Wall)
						break;
					if(map[x][curPos.y] == tile)
						retVal++;
				}

				break;
			case Orient.South:
				for (int i = 1, y = curPos.y + 1; true; i++, y++) {
					if (map[curPos.x][y] == Tile.Wall)
						break;
					if(map[curPos.x][y] == tile)
						retVal++;
				}

				break;
			case Orient.West:
				for (int i = 1, x = curPos.x - 1; true; i++, x--) {
					if (map[x][curPos.y] == Tile.Wall)
						break;
					if(map[x][curPos.y] == tile)
						retVal++;
				}

				break;
		}
		return retVal;
	}
	/**
	 * Whether there is the provided tile on the left of the robot
	 * @param tile The tile to test
	 * @return Wheter it is there
	 */
	public boolean isLeft(int tile){
		int orient = Orient.Edit(curOrient, -1);
        return isOn(tile, orient);
	}

    /**
     * Whether there is the provided tile on the right of the robot
     * @param tile The tile to test
     * @return Wheter it is there
     */
    public boolean isRight(int tile){
        int orient = Orient.Edit(curOrient, 1);
        return isOn(tile, orient);
    }

    /**
     * Whether there is the provided tile in the robot's orientation
     * @param tile The tile to test
     * @param orient Robot's orientation
     * @return Wheter it is there
     */
    private boolean isOn(int tile, int orient) {
        switch (orient) {
        case Orient.North:
            if (map[curPos.x][curPos.y-1] == tile) {
                return true;
            }
            break;
        case Orient.East:
            if (map[curPos.x+1][curPos.y] == tile) {
                return true;
            }
            break;
        case Orient.South:
            if (map[curPos.x][curPos.y+1] == tile) {
                return true;
            }
            break;
        case Orient.West:
            if (map[curPos.x-1][curPos.y] == tile) {
                return true;
            }
            break;
        }
        return false;
    }

	
	/**
	 * Find path to nearest request by using Breadth-first search. 
	 * @param start
	 *            Starting position
	 * @param request
	 *            Requested tile
	 * @param reject
	 *            Tiles to reject e.g. wall
	 * @return Shortest path to request
	 */
	@SuppressWarnings("deprecation")
	public Path BFSearch(Pos start, int request, int[] reject) throws InterruptedException {
		// INICIALIZACE
		java.util.Map<Pos, Pos> parent = new HashMap<>(); // Pole rodičů
		parent.put(start, Pos.none); // Start je kořen

		List<Pos> frontier = new ArrayList<>(); // Otevřené uzly
		frontier.add(start); // Start je první
		
		Pos nearest = Pos.none;
		// PRŮCHOD GRAFEM (MAPOU)
		try { // Zachycení nalezení světla
			while (frontier.size() != 0) // Dokud máme co procházet
			{
				List<Pos> next = new ArrayList<>(); // Budoucí průchod
				for (Pos u : frontier) // Projdeme otevřené uzly
				{
                    Pos[] adjacent = Pos.adjacent(u);
					for (Pos v : adjacent) // Projdeme okolí uzlu
					{
						if (!(parent.containsKey(v) || Common.aContainsO(map[v.x][v.y], reject)))
								// Pokud máme fresh uzel a ten není zábrana
						{
							parent.put(v, u); // Nastavit rodiče -> navštíven
							if (map[v.x][v.y] == request) { 
								// Pokud jsme narazili na světlo
								nearest = v; // přiřadit
								throw new FoundException(); // TADÁ ODSUD
							}
							next.add(v); // Přidat do dalšího průchodu
						}
					}
				}
				frontier = next; // přeřazení, uzavření frontierů
			}
		} 
		catch(FoundException ignored){} // TADÁ SEM
		// HLEDÁNÍ CESTY
		if(nearest == Pos.none) // Pokud už nejsou žádná známá světla
			return null;
		
		Stack<Pos> path = new Stack<>();
		Pos explorPos = nearest;
		while(explorPos != Pos.none)
		{
			path.push(explorPos); // Přidat uzel
			explorPos = parent.get(explorPos); // Posunout se dopředu
		}
		Queue<Pos> retVal = new LinkedList<>(); // EDIT
		while(path.size()!=0)
			retVal.add(path.pop());
		return new Path(retVal); // Vrátit cestu

	}

    // INNER TYPES & PSEUDO-ENUMS

	/**
	 * Orientation abstraction
	 * 
	 * @author kuba
	 */
	public static class Orient {
		private Orient() {}

		public static final int North = 0;
		public static final int East = 1;
		public static final int South = 2;
		public static final int West = 3;

		/**
		 * Modified orientation
		 * 
		 * @param actual
		 *            Actual orientation
		 * @param edit
		 *            Turn
		 * @return Turned orientation
		 */
		public static int Edit(int actual, int edit) {
			return (actual + edit + 4) % 4;
		}

		/**
		 * Returns absolute orientation of ultrasonic sensor
		 * 
		 * @param actual
		 *            Actual orientation
		 * @param usPos
		 *            Sensor orientation aka nxjsim.AR.Mechanics.Direction
		 * @see Mechanics.Direction
		 * @return Absolute orientation of ultrasonic sensor
		 */
		public static int usEdit(int actual, int usPos) {
			return Edit(actual, usPos - 1);
		}
		
		/**
		 * Get optimal turning between two orientations
		 * @param first Original orientation
		 * @param second New orientation
		 * @return Optimal turning (clockwise is plus)
		 */
		public static int difference(int first, int second)
		{
            if(first == second)
                return 0;
            while(first != 0 && second != 0)
            {
                first--;
                second--;
            }
            if(first < second) // first decision -> turn clockwise
            {
                if(second == 1)
                    return 1;
                if(second == 2)
                    return 2;
                if(second == 3)
                    return -1;
            }
            else // first decision ->  turn c-clockwise
            {
                if(first == 1)
                    return -1;
                if(first == 2)
                    return 2;
                if(first == 3)
                    return 1;
            }
            return 0;
		}
	}

	/**
	 * Tile abstraction
	 * 
	 * @author Jakub Vaněk
	 */
	public static class Tile {
		private Tile() {}

		public static final int Unknown = 0;
		public static final int Wall = 1;
		public static final int LightOn = 2;
		public static final int LightOff = 3;
	}
}
